<?php
/**
 * This script was written for a tool used to create, manage and take surveys integrated into a custom CMS in CakePHP.
 */

App::uses('AuthComponent', 'Controller/Component');

class SurveysController extends AppController {

	var $uses = array('Survey', 'Response');

	public function beforeFilter(){
		parent::beforeFilter();
	}

	public function admin_index(){
		if ($this->request->is(array('post', 'put'))) {
			$this->Survey->create();
			if ($this->Survey->save($this->data)) {
				$this->Session->setFlash('Survey created. You can now create a group for it.', 'default', array(), 'create_group');
				$this->redirect(array('controller'=>'SurveyGroups', 'action'=>'index', '#'=>'survey-group-form'));
			}
		}
		$surveys = $this->Survey->find('all', array('recursive'=>-1));

		$this->set('surveys', $surveys);
	}
	
	public function clients_index(){
		if ($this->request->is(array('post', 'put'))) {
			$accessCode = strtolower($this->data['Survey']['SurveyGroup']['access_code']);
			$survey     = $this->Survey->SurveyGroup->find('first', array('conditions'=>array('SurveyGroup.survey_access_code'=>$accessCode)));

			if ($survey) {
				$response = $this->Survey->SurveyGroup->Response->find('first', array('conditions'=>array(
																								'Response.user_id'   	   => $this->Auth->user('id'),
																								'Response.survey_group_id' => $survey['SurveyGroup']['id']
																							)));

				$accessSurvey = true;
				if (empty($response)) {
					// User accessed this survey for the first
					// time so we create a Response record
					$this->request->data['survey_group_id'] = $survey['SurveyGroup']['id'];
					$this->request->data['user_id']			= $this->Auth->user('id');

					$this->Survey->SurveyGroup->Response->create();
					$this->Survey->SurveyGroup->Response->save($this->request->data);
					
				} elseif ($response['Response']['completed'] == 1) {

					// Survey has already been submitted so cannot access it again
					$this->Session->setFlash(__('Your already have completed this survey.', true));
					$accessSurvey = false;

				}

				if ($accessSurvey === true) {
					// Check which category and questions this user can see
					$dataAr = $this->nextCategoryFound($survey['Survey']['id'], $survey['SurveyGroup']['id']);
					
					// redirect to the appropriate category
					return $this->redirect(array(
											'controller' => 'surveys',
											'action'     => 'questions',
											'clients'	 => true,
											$dataAr['survey_id'],
											$survey['SurveyGroup']['id'],
											$dataAr['category_id']
											));	
				}

			} else {
				$this->Session->setFlash(__('The access code submitted hasn\'t been recognised.', true));
			}
		}
	}
	
	private function nextCategoryFound($surveyId, $surveyGroupId, $nextCtg=1){
		// $nextCtg is the next category in the
		// array which will be ordered by sequence

		// Get next category section using sequence so we can change order if needed
		$options = array(
						'conditions' => array('Category.survey_id'=>$surveyId),
						'order'		 => 'Category.sequence ASC',
						'recursive'	 => 0
						);
		
		$categoryAr = $this->Survey->Category->find('all', $options);

		// Get the correct data in array
		$dataAr = $this->getDataSurvey($categoryAr[$nextCtg-1]['Category']['survey_id'], $categoryAr[$nextCtg-1]['Category']['id'], $surveyGroupId, $categoryAr);

		if(!$dataAr) {
			// If false is returned this category is
			// not needed so we check the next one
			$dataAr = $this->nextCategoryFound($surveyId, $surveyGroupId, $nextCtg+1);
		}
		
		return $dataAr;
	}

	private function getDataSurvey($survey_id, $category_id, $survey_group_id, $categories){
		// Get all the categories this user has access to
		// and all the questions for the current category

		// Check if there is any record in category_clients table for this user for this survey
		$ctgUserRecords = $this->Survey->Category->CategoryUser->find('all', array('conditions'=>array(
																									'CategoryUser.survey_group_id' => $survey_group_id,
																									'CategoryUser.user_id' => $this->Auth->user('id'),
																									'Category.survey_id'   => $survey_id
																								)));
		if (!empty($ctgUserRecords)) {

			foreach ($ctgUserRecords as $ctgUserRecord) {
				if (empty($ctgUserRecord['CategoryUsersQuestion'])) {
					if ($ctgUserRecord['Category']['id'] == $category_id) {
						// If this condition is met these is no need to carry on
						// as this means the entire category will be skipped
						return false;
					} else {
						foreach ($categories as $key=>$category) {
							if ($category['Category']['id'] == $ctgUserRecord['Category']['id']) {
								// Unset all the categories this user doesn't need to see
								unset($categories[$key]);
							}
						}
					}
				} elseif ($ctgUserRecord['Category']['id'] == $category_id) {
					// Get only selected question IDs for this client
					$questionIds = array();
					foreach ($ctgUserRecord['CategoryUsersQuestion'] as $question) {
					
						if ($ctgUserRecord['CategoryUser']['id'] == $question['category_user_id']) {
								$questionIds[] = $question['question_id'];
						}
					}

					if (isset($questionIds)) {
						$questionsOpt['conditions']['Question.id'] = $questionIds;
					}
				}
			}
		}
		
		$dataAr['survey_id'] 		   = $survey_id;
		// Category to display
		$dataAr['category_id'] 		   = $category_id;
		$dataAr['survey_group_id']     = $survey_group_id;
		// All categories this user will see
		$dataAr['categories_selected'] = $categories;

		$questionsOpt['conditions']['Question.survey_id']   = $survey_id;
		$questionsOpt['conditions']['Question.category_id'] = $category_id;
		$questionsOpt['order']['Question.sequence'] = 'ASC';
		$questionsOpt['recursive'] = -1;
		// All questions of this category this user will see
		$dataAr['questions_selected']  = $this->Survey->Question->find('all', $questionsOpt);
		
		return $dataAr;
	}

	public function clients_questions($survey_id=NULL, $survey_group_id=NULL, $category_id=NULL){
		// If Survey_group id is not provided and the
		// survey is accessed without using the form
		
		if (!$this->request->is(array('post','put')) && !$survey_group_id) {
			$this->Session->setFlash(__('Sorry this page does not exist.', true));
			return $this->redirect(array('controller'=>'surveys', 'action'=>'index', 'clients'=>true));
		}
		
		$response = $this->Survey->SurveyGroup->Response->find('first', array('conditions'=>array(
													'Response.user_id'   	   => $this->Auth->user('id'),
													'Response.survey_group_id' => $survey_group_id
													)));
		// If no response record found and user hasn't
		// used the form to access the survey
		if (!$this->request->is(array('post','put')) && empty($response)) {
			$this->Session->setFlash(__('You need to use this form before accessing the survey.', true));
			return $this->redirect(array('controller'=>'surveys', 'action'=>'index', 'clients'=>true));
		}
	
		// Get categories and questions this user can see
		
		if (!empty($category_id)) { // $category_id is empty when submiting the survey
			$dataAr = $this->nextCategoryFound($survey_id, $survey_group_id, $category_id);
		}

		if ($this->request->is(array('post','put'))) {

			foreach ($this->data['ResponseAnswer'] as $question_id => $answerAr) {

				$arrayKey  = array_keys($answerAr);
				$keyString = array_shift($arrayKey);
				
				// If <select> element and nothing was selected it will 
				// send 'Select' as a value so we skip that
				if (!empty($answerAr[$keyString]) && ($answerAr[$keyString] != 'Select')) {

					$options = array('conditions'=>array(
										'ResponseAnswer.response_id' => $response['Response']['id'],
										'ResponseAnswer.question_id' => $question_id
										));
					$responseAnswer = $this->Survey->SurveyGroup->Response->ResponseAnswer->find('first', $options);

					$this->request->data['ResponseAnswer']['response_id'] = $response['Response']['id'];
					$this->request->data['ResponseAnswer']['question_id'] = $question_id;
					$this->request->data['ResponseAnswer'][$keyString]    = $answerAr[$keyString];
					
					if (empty($responseAnswer)) {
						$this->Survey->SurveyGroup->Response->ResponseAnswer->create();
					} else {
						$this->request->data['ResponseAnswer']['id'] = $responseAnswer['ResponseAnswer']['id'];
					}

					$this->Survey->SurveyGroup->Response->ResponseAnswer->save($this->request->data);

					unset($this->request->data['ResponseAnswer'][$keyString]);
					unset($this->request->data['ResponseAnswer']['id']);
				}
				
				// Update date in the Response table
				$this->Survey->SurveyGroup->Response->save(array('Response'=>array('id'=>$response['Response']['id'])));
			}
			
			if (isset($this->params['named']['submit'])) {
				$this->Survey->SurveyGroup->Response->id = $response['Response']['id'];
				$this->Survey->SurveyGroup->Response->saveField('completed', 1);
				
				$this->send_email();
				return $this->redirect(array('controller'=>'surveys', 'action'=>'complete', 'clients'=>true));
			}
		}
		
		$types      = $this->Survey->SurveyGroup->Response->ResponseAnswer->getFieldEnums('range_answer', false);
		$conditions = array('Question.survey_id'=>$survey_id, 'Question.category_id'=>$category_id);
		$questions  = $this->Survey->Question->find('all', array('conditions'=>$conditions));

		if ($questions) {

			// Get number of categories for this user
			$countCtgs = count($dataAr['categories_selected']);
					
			$onStage = 1;
			foreach ($dataAr['categories_selected'] as $ctg) {
				if ($ctg['Category']['id'] == $category_id) {
					$currentCategory = $ctg['Category'];
					break;
				}
				$onStage++;
			}

			foreach ($questions as $question) {
				$question_id = $question['Question']['id'];
				$conditions  = array('recursive'=>-1, 'conditions'=>array(
													'ResponseAnswer.response_id'=>$response['Response']['id'],
													'ResponseAnswer.question_id'=>$question_id)
													);
				$answer = $this->Survey->SurveyGroup->Response->ResponseAnswer->find('first', $conditions);
				if ($answer) {
					$this->request->data['ResponseAnswer'][$question_id][$question['Question']['type'] .'_answer'] = $answer['ResponseAnswer'][$question['Question']['type'] .'_answer'];
				}
			}

			$this->set(compact('dataAr', 'survey_group_id', 'category', 'types', 'countCtgs', 'onStage', 'currentCategory'));
		}
	}


	public function send_email(){
		//enable sending email
		$this->_useEmail();
		
		//get the respondee/user display name or email
		$name	= $this->Auth->user('name');
		$user	= !empty($name) ? $name : $this->Auth->user('email');
		
		# --- send respondee confirmation email ---
		$to		= array($this->Auth->user('email') => $user);
		$subject= 'Survey Submission Confirmation';
		//build the message
		$html	= '<p>Thank you for taking the time to complete our client survey. We very much value your feedback.<br />'."\n"
				. 'If we can be of any assistance, please do not hesitate to contact the team.<br /><br />'."\n\n"
				. 'Best wishes<br /><br />';
		$text	= strip_tags($html);
		//send the respondee email
		$sent = $this->qemail
			->subject($subject)
			->to($to)
			->htmlBody($html)
			->textBody($text)
			->qSend();
		//var_dump($sent);
		//die();
		
		# --- Send admin email ---
		$to		= array(
			'email@gmail.uk'=>'Sender email'
		);
		$subject= 'Survey Submitted';
		//get the admin url to view the survey response
		$url = Router::url(array('clients'=>false, 'admin'=>true, 'controller'=>'Responses', 'action'=>'view', $this->Survey->SurveyGroup->Response->id,'full_base'=>true));
		//build the message
		$html	= '<p>A completed survey has been submitted by '. $user.'.<br />'."\n"
				. '<a href="'.$url.'">'.$url.'</a></p>';
		$text	= strip_tags($html);
		//send the admin email
		$sent = $this->qemail
			->subject($subject)
			->to($to)
			->htmlBody($html)
			->textBody($text)
			->qSend();
	}
	
	public function clients_complete(){

	}

}