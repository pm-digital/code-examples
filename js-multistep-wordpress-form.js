// This script is to create a multi-step form which can be found on this website https://www.hmlgroup.com/communication-preference-form/
jQuery(document).ready(function($){
	//Add ID attribute to checkboxes as CF7 doesn't allow that
	$('#pref-form input:checkbox').each(function(){
		var $this = $(this);
		var name = $this.attr('name');
		$this.attr('id',name);
	});
	  
	//Mask plugin setup
	$("#utr").mask('T00-000000-00', {placeholder: 'T00-000000-00'});

	//Open section to edit
	$('body').on('click', '.step-editable .step-header', function() {
		var $this = $(this);
		var openedElem = $this.closest('.step-editable');
		//Remove all '.step-editable' so user can't jump to
		//another section without validating the current one
		$('.form-step').removeClass('step-editable');
		// $('.form-step').removeClass('current-step');
		$('.step-content').slideUp();
		$("#pref-form .wpcf7-validates-as-required").prop('required',true);

		openedElem.addClass('updating-step');
		openedElem.find('.step-content').slideDown();
	});
	//Validate each section when clicking the 'Next' button
	//if valid then go to next step
	$(".next-step").on('click', function(){
		var $this = $(this);
		// var currentElem = $this.closest('.current-step');
		var currentElem = $this.closest('.form-step');
		var isValid = true;
		// CHECK Tenant Reference format
		// check that length is 13 characters
		var tenantNumField = currentElem.find('#utr:visible');
		if (tenantNumField.length){
			var numVal = tenantNumField.val();
			if (numVal.length != 13) {
				isValid = false;
				$('.invalidformat').show();
			}else{
				$('.invalidformat').hide();
			}
		}

		var contactInfo = currentElem.find('.contact-info-section:visible');
		var fieldsNotEmpty = true;
		var isNumber = true;
		var checkboxesNotEmpty = true;
		if (contactInfo.length){
			contactInfo.each(function(){
				fieldsNotEmpty = false;
				isNumber = false;
				checkboxesNotEmpty = false;
				//Validate 'tel' inputs
				var telNumIndex = '';
				var telNumAmount = 0;
				var $infoSection = $(this);
				var inputElems = $infoSection.find("input[type='tel']");
				inputElems.each(function(index, value){
					if($(this).val() != ''){
						fieldsNotEmpty = true;
						telNumIndex = index;
						if($.isNumeric($(this).val())){
							isNumber = true;
						}
						telNumAmount++;
					}
				});
				// If only one number, check it as emergency contact
				if (telNumAmount==1) {
					var telInputId = inputElems[telNumIndex].id;
					$('#emergency-'+telInputId).prop('checked', true);
				}
				if (fieldsNotEmpty){$('.noinputvalid').hide();}
				else {$('.noinputvalid').show();}
				//Validate 'checkbox' inputs
				var checkboxElems = $infoSection.find("input:checkbox:checked");
				console.log(checkboxElems.length);
				if(checkboxElems.length >= 1){
					checkboxesNotEmpty = true;
				}
				if (isNumber){$('.notanumber').hide();}
				else {$('.notanumber').show();}
				if(checkboxElems.length > 1){$('.toomanychecked').show();isValid=false}
				else {$('.toomanychecked').hide();}
				
				if (checkboxesNotEmpty){$('.nocheckboxvalid').hide();}
				else {$('.nocheckboxvalid').show();}

				if (fieldsNotEmpty && checkboxesNotEmpty && isValid && isNumber){
					$infoSection.removeClass('novalide');
				} else {
					$infoSection.addClass('novalide');
				}
			});
		}
		
		var currentElemId = currentElem.attr('id');
		var form = $( "#pref-form" );
		form.validate();
		if (form.valid() && fieldsNotEmpty && checkboxesNotEmpty && isNumber && isValid) {
			//Get next visible section ID
			var nextStepId = currentElem.nextAll(".uncomplete-step:visible").first().attr('id');
			if (currentElem.hasClass('completed-step')){
				// Go back to section where we left off
				$('.updating-step .step-content').slideUp();
				$('.updating-step').removeClass('updating-step');
			} else {
				//Hide current section and show edit button
				$('.current-step .step-content').slideUp();
				$('.current-step').addClass('completed-step').removeClass('current-step, uncomplete-step');
			}
				$('.completed-step').addClass('step-editable');
			//Expand next section
			$("#"+ nextStepId +" .step-content").slideDown(function(){
				$('html, body').animate({
        			scrollTop: $("#"+ nextStepId).offset().top - 60
    			}, 500);
			});
			$("#"+ nextStepId).addClass('current-step');
			
			//If at review step retrive data and display
			if (nextStepId == 'review-submit-step') {
				reviewData();
			}
		}
	});
	function reviewData(){
		//Change text of buttons
		$(".next-step").text('Update');
		//Show edit buttons
		$('.completed-step').addClass('step-editable');
		//Remove 'requited' attributs to pass hidden field validation on submit
		$("#pref-form .wpcf7-validates-as-required").prop( "required", null );
		//Loop through all .review-field element in Review section
		$('#review-submit-step .review-field').each(function(){
			var $this = $(this);
			var reviewElemId = $this.attr('id');
			//Remove 'review-' from value and we get the field ID
			var fieldId = reviewElemId.replace('review-','');
			//var inputVal = $('input[name ="'+fieldId+'"]').val();
			var inputVal = "";
			//Check if 'data-ischeckbox' exists in the field
			var isCheckbox = lookForAttr($this, 'data-ischeckbox');
			//Get value entered in the field
			if (isCheckbox != "") {
				var array = '';
				if (isCheckbox.indexOf("|") >= 0) {
					//If '|' character found then value before is when checked
					//and value after that is when unchecked. Ex.: Yes|No
					var array = isCheckbox.split('|');
				}

				if($('#'+fieldId).is(":checked")) {
					inputVal = $.isArray(array) ? array[0] : isCheckbox;
				} else {
					inputVal = $.isArray(array) ? array[1] : "";
				}
			} else {
				inputVal = $('#'+fieldId).val();
			}
			//Show value in the div
			$this.html(inputVal);
		});
	}
	//Switch next step to be opened
	switchSection($('select#ismainresidence option:selected').val());
	$('select#ismainresidence').on('change', function() {
		switchSection(this.value);
	});
	function switchSection(elem){
		if (elem == 'Yes'){
			$('#rev-alternative').hide();
			$('#tenancy-info-step').slideUp();
			$('#rev-tenancy-info').hide();
		} else if (elem == 'No, Other') {
			$('#tenancy-info-step').slideUp();
			$('#rev-tenancy-info').hide();
		} else {
			$('#rev-alternative').show();
			$('#tenancy-info-step').slideDown();
			$('#rev-tenancy-info').show();
			$('#tenancy-info-step').slideDown();
			$('#rev-tenancy-info').show();
		}
	}
	// Show hide Other contact section
	switchVisibility($('#skip-othercontact input'), '#other-contact-step');
	$('#skip-othercontact input').on('change', function() {
		switchVisibility($(this), '#other-contact-step');
	});
	switchVisibility($('#consent-reviewsubmit-wrap input'), '#form-valid-show-bt');
	$('#consent-reviewsubmit-wrap input').on('change', function() {
		switchVisibility($(this), '#form-valid-show-bt');
	});
	function switchVisibility(elem, target){
		if(elem.is(":checked")) {
            $(target).slideDown();
        }else{
            $(target).slideUp();
        }
	}
	
	//Check if specified attribute exist
	function lookForAttr(elem, attrName){
		var attrVal = "";
		var attr = elem.attr(attrName);
		// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
		if (typeof attr !== typeof undefined && attr !== false) {
		  attrVal = attr;
		}
		return attrVal;
	}
	//Add 'required' attribute
	$("#pref-form .wpcf7-validates-as-required").prop('required',true);
	$("#pref-form").removeAttr('novalidate');
	
});